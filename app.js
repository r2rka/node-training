import express from 'express';
import env  from 'dotenv';
import './dbconfig';
import routes from './routes';
env.config();
import api from './api/routes';
import bodyParser from "body-parser";

const app = express();

app.use(bodyParser.urlencoded({
  extended: true
}));

app.use(bodyParser.json());

app.listen(process.env.SERVER_PORT, () => {
  console.log(`Server Started at port: ${process.env.SERVER_PORT}`)
});

/** Initialize vies routes */

routes(app);

/**Initialize api routes*/

api(app);

