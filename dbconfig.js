import 'dotenv/config';
import mongoose from 'mongoose';
import './models';

mongoose.connect(`mongodb://${process.env.DB_URL}:${process.env.DB_PORT}/${process.env.DB_COLLECTION}`, { useNewUrlParser: true, useUnifiedTopology: true });

mongoose.connection.on('connected', () => {
  console.log(`DB connected to mongodb://${process.env.DB_URL}:${process.env.DB_PORT}/${process.env.DB_COLLECTION}`)
});