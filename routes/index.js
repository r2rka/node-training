import path from 'path';
const appDir = path.dirname(require.main.filename);

export default (app) => {

  app.get('/', (req, res) => {
    res.sendFile(path.join(appDir, '/views/login.html'))
  })

  app.get('/register', (req, res) => {
    res.sendFile(path.join(appDir, '/views/register-form.html'))
  })

};