import mongoose from 'mongoose'
const Schema = mongoose.Schema

const schema =  new Schema({
  login: String,
  password: String
})

export default mongoose.model('User', schema)