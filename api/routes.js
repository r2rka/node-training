import User from '../models/User'


export default app => {

app.post('/register', async (req, res, next) => {
  const user = new User({ ...req.body });

  await user.save();

  next('/login')
})

}